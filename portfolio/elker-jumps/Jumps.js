import axios from 'axios'

export default {
  async get({ subdomain, id }) {
    const response = await axios.get(`/organizations/${subdomain}/user_steps/${id}/jumps`)
    return response.data
  },

  async update({ subdomain, id, jumps }) {
    const response = await axios.put(`/organizations/${subdomain}/user_steps/${id}/jumps`, { jumps })
    return response.data
  }
}

