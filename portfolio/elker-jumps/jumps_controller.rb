module Api
  module Saas
    module Organizations
      class JumpsController < ::Api::Saas::BaseController
        before_action :switch_organization!

        def index
          represented = representer.for_collection.new(step.jumps)
          respond_success(jumps: represented)
        end

        def update
          validator = ::Jumps::UpdateValidator.new(step: step, jumps: jump_params[:jumps])

          if validator.valid?
            service = Jumps::Update.call(step: step, jumps: jump_params[:jumps])
            represented = representer.for_collection.new(service.result)
            respond_success(jumps: represented)
          else
            respond_fail(errors: validator.error_codes)
          end
        end

        private

        def step
          step ||= Step.find(params[:id])
        end

        def jump_params
          params.permit(:jumps => [:step_id, :matcher, :text, :option_id, :next_step_id])
        end

        def representer
          ::Web::Reports::JumpRepresenter
        end
      end
    end
  end
end
