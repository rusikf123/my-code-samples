module Jumps
  class UpdateValidator < ::ApplicationValidator
    attr_accessor :jumps, :step

    validate :jumps_format

    def jumps_format
      invalid_jumps = jumps.find { |jump| Step.find_by(id: jump[:next_step_id]).blank? }
      errors.add(:base, "Next step can't be blank") if invalid_jumps.present?
    end
  end
end
