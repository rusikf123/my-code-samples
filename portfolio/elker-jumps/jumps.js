import api from '@/api/admin/Jumps'

export default {
  namespaced: true,

  state: {
    list: [],
    loading: false
  },

  mutations: {
    setLoading(state, payload) {
      state.loading = payload
    },
    resetJumps(state) {
      state.list = []
    },
    setJumps(state, payload) {
      state.list = payload
    },
    updateJumpById(state, payload) {
      const currentJump = state.list.find(item => item.id == payload.id)
      const index = state.list.indexOf(currentJump)

      let newJump = {...currentJump, ...payload }
      state.list = [
        ...state.list.slice(0, index),
        newJump,
        ...state.list.slice(index + 1)
      ]
    },
    removeJump(state, id) {
      let index = state.list.findIndex(jump => jump.id === id)
      state.list.splice(index, 1)
    },
    addJump(state, payload) {
      state.list.push(payload)
    }
  },

  actions: {
    async fetchJumps({ commit }, { subdomain, id }) {
      commit('setLoading', true)
      const { jumps } = await api.get({ subdomain, id })
      commit('setJumps', jumps)
      commit('setLoading', false)
    }
  }
}
