import { useSelector } from 'react-redux'
// Action types
export const SET_WATERFALL = 'SET_WATERFALL'
export const TOGGLE_ROW_DETAILS = 'TOGGLE_ROW_DETAILS'
export const SET_FILTER = 'SET_FILTER'
// Actions
export const setWaterfall = waterfall => ({ type: SET_WATERFALL, waterfall })

export const toggleRowDetails = id => ({ type: TOGGLE_ROW_DETAILS, id })
export const setFilter = text => ({ type: SET_FILTER, text })

//Reducers

const initialState = {
  timeline: {
    result: []
  },
  rows: [],
  filterText: ''
}

const hasText = (html, text) => {
  return html && html.includes(text)
}

const setFiltered = ({rows, filtered, detailsVisible}) => {
  return rows.map(row => {
    return { ...row, detailsVisible, filtered }
  })
}

const filterWaterfall = (state, action) => {
  const { text } = action
  const { rows } = state
  let newRows
  if (text && text.length > 0) {
    newRows = rows.map(row => {
      const { url, headers } = row
      const { request, response } = headers
      const filtered = hasText(url, text) || hasText(request, text) || hasText(response, text)
      return { ...row, detailsVisible: filtered, filtered }
    })
  } else {
    newRows = setFiltered({ rows, filtered: true, detailsVisible: false })
  }

  return {
    ...state,
    filterText: text,
    rows: newRows
  }
}

export const waterfallList = (state = initialState, action) => {
  if (action.type === SET_WATERFALL) {
    const { rows, max, timeline } = action.waterfall
    return  {
      ...state,
      rows,
      max,
      timeline,
    }
  }

  if (action.type === TOGGLE_ROW_DETAILS) {
    const index = state.rows.findIndex(row => action.id === row.id)
    let rows = [...state.rows]
    const row = rows[index]
    rows[index] = { ...row, detailsVisible: !row.detailsVisible }
    return { ...state, rows }
  }

  if (action.type === SET_FILTER) {
    return filterWaterfall(state, action)
  }

  return state
}

// Selectors

const useWaterfall = () => useSelector(state => state.waterfall)
export const useRows = () => useWaterfall().rows
export const useFilteredRows = () => useRows().filter(row => row.filtered)
export const useMax = () => useWaterfall().max
export const useTimeline = () => useWaterfall().timeline
export const useStep = () => useTimeline().step
export const useFilterText = () => useWaterfall().filterText

