import React from 'react'
import { useTimeline } from '@/app/redux/modules/waterfall'

export default function Timeline() {
  const timelineData = useTimeline()

  const rows = timelineData.result.map((time, index) => {

    const offset = `${52 * index}px`
    return (
      <>
        <span className="step" style={{ left: offset }}>{ time }</span>
      </>
    )
  })
  return (
    <div className="step-container">
      { rows }
    </div>
  )
}

