export const useHighlight = ({text, query }) => {
  if (!text) {
    return { __html: text }
  }
  return { __html: text.replace(query, `<span class='highlight-phrase'>${query}</span>`) }
}


